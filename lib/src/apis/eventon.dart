import 'dart:convert';
import 'package:evensear_app/src/models/event.dart';
import 'package:http/http.dart';

class Eventon {
  final String baseUrl = 'https://eventon.jp/api/events.json';

  Future<List<Event>> fetchEvents(
      {String datetime, String keyword, int prefectureId, String place}) async {
    Map<String, String> params = {
      'limit': '100',
      'ymd': datetime,
      'keyword': keyword,
      'prefecture_id': prefectureId == null || prefectureId == 0 ? '' : prefectureId.toString(),
      'place': place
    };
    String uri = Uri.https('eventon.jp', '/api/events.json', params).toString();
    try {
      final Response response = await get(uri);
      final Map<String, dynamic> decodedJson = json.decode(response.body);
      final List data = decodedJson['events'];
      final List<Event> eventsList = data.map((event) => Event(event)).toList();
      return eventsList;
    } catch (err) {
      print(err);
      return null;
    }
  }
}
