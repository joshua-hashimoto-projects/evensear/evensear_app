import 'package:flutter/material.dart';

class CustomIconButtonWidget extends StatelessWidget {
  final Function onTap;
  final IconData icon;

  CustomIconButtonWidget({@required this.icon, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      height: 40.0,
      child: GestureDetector(
        onTap: onTap,
        child: CircleAvatar(
          radius: 10.0,
          backgroundColor: Colors.grey.withOpacity(0.5),
          child: Icon(
            icon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
