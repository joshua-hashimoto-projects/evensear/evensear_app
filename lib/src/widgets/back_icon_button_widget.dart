import 'package:evensear_app/src/widgets/custom_icon_button_widget.dart';
import 'package:flutter/material.dart';

class BackIconButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 40.0,
      left: 10.0,
      child: CustomIconButtonWidget(
        icon: Icons.arrow_back_ios,
        onTap: () => Navigator.pop(context),
      ),
    );
  }
}
