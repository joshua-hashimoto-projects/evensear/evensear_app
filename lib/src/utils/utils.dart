import 'package:url_launcher/url_launcher.dart';

Future<void> urlLaunch({String uri}) async {
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    print('this uri is not available');
  }
}
