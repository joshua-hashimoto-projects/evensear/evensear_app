import 'dart:async';
import 'package:evensear_app/src/models/event.dart';

class Validators {
  final validateEvents =
      StreamTransformer<List<Event>, List<Event>>.fromHandlers(handleData: (events, sink) {
    if (events.isNotEmpty) {
      sink.add(events);
    } else {
      sink.addError('No data');
    }
  });
}
