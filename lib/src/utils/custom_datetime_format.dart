import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class CustomDatetimeFormat {
  static String plainYmd(DateTime date) {
    initializeDateFormatting('ja_JP');
    DateFormat formatter = DateFormat('yyyyMMdd', 'ja_JP');
    String formattedDatetime = formatter.format(date);
    return formattedDatetime;
  }

  static String slashYmd(DateTime date) {
    initializeDateFormatting('ja_JP');
    DateFormat formatter = DateFormat('yyyy/MM/dd', 'ja_JP');
    String formattedDatetime = formatter.format(date);
    return formattedDatetime;
  }

  static String plainTime(DateTime date) {
    initializeDateFormatting('ja_JP');
    DateFormat formatter = DateFormat('HH:mm a', 'ja_JP');
    String formattedDatetime = formatter.format(date);
    return formattedDatetime;
  }
}
