import 'dart:async';
import 'package:evensear_app/src/apis/eventon.dart';
import 'package:evensear_app/src/blocs/events_bloc.dart';
import 'package:evensear_app/src/models/event.dart';
import 'package:evensear_app/src/screens/home/widgets/bottomreveal.dart';
import 'package:evensear_app/src/screens/home/widgets/calendar_strip_widget.dart';
import 'package:evensear_app/src/screens/home/widgets/event_cell.dart';
import 'package:evensear_app/src/screens/home/widgets/menu_widget.dart';
import 'package:evensear_app/src/screens/home/widgets/no_events_widget.dart';
import 'package:evensear_app/src/screens/home/widgets/search_bar_widget.dart';
import 'package:evensear_app/src/screens/info/info_screen.dart';
import 'package:evensear_app/src/utils/custom_datetime_format.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final BottomRevealController _bottomRevealController = BottomRevealController();
  final Eventon _eventon = Eventon();
  final DateTime _calendarStart = DateTime.now().subtract(Duration(days: 365 * 5));
  final DateTime _calendarEnd = DateTime.now().add(Duration(days: 365 * 5));
  DateTime _selectedDate = DateTime.now();
  String _searchTerm = '';

  void _fetchData({DateTime date, String term}) async {
    final String _formattedDatetime = CustomDatetimeFormat.plainYmd(date);
    final List<Event> _response =
        await _eventon.fetchEvents(datetime: _formattedDatetime, keyword: term);
    eventsBloc.changeEvents(_response);
  }

  Future _selectDate(BuildContext context) async {
    final DateTime _selected = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: _calendarStart,
      lastDate: _calendarEnd,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: child,
          ),
        );
      },
    );
    if (_selected != null) {
      setState(() {
        _selectedDate = _selected;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Event>>(
      stream: eventsBloc.events,
      builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
        return Scaffold(
          body: BottomReveal(
            openIcon: Icons.search,
            closeIcon: Icons.close,
            revealWidth: 100.0,
            revealHeight: 100.0,
            backColor: Colors.grey[400],
            frontColor: Colors.grey.shade200,
            rightContent: _menuWidget(),
            bottomContent: _searchBarWidget(),
            controller: _bottomRevealController,
            body: SafeArea(
              child: Stack(
                children: <Widget>[
                  !snapshot.hasData ? NoEventsWidget() : _eventListWidget(snapshot),
                  _calendarStripWidget(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _menuWidget() {
    return MenuWidget(
      onInfoTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InfoScreen(),
            fullscreenDialog: true,
          ),
        );
      },
      onCalendarTap: () async {
        await _selectDate(context);
        _fetchData(date: _selectedDate, term: _searchTerm);
        _bottomRevealController.close();
      },
    );
  }

  Widget _searchBarWidget() {
    return SearchBarWidget(
      onTextChange: (val) {
        setState(() {
          _searchTerm = val;
        });
      },
      onEditFinish: () {
        _fetchData(date: _selectedDate, term: _searchTerm);
        FocusScopeNode currentFocus = FocusScope.of(context);
        currentFocus.unfocus();
        _bottomRevealController.close();
      },
    );
  }

  Widget _eventListWidget(AsyncSnapshot<List<Event>> snapshot) {
    return ListView.builder(
      padding: EdgeInsets.only(
        left: 16.0,
        top: 130.0,
        right: 16.0,
        bottom: 16.0,
      ),
      key: ObjectKey(snapshot.data[0]),
      itemCount: snapshot.data.length,
      itemBuilder: (context, int index) {
        return EventCell(
          use: snapshot.data[index],
        );
      },
    );
  }

  Widget _calendarStripWidget() {
    return CalendarStripWidget(
      selectedDate: _selectedDate,
      onMonthTap: () async {
        _bottomRevealController.open();
        await _selectDate(context);
        _fetchData(date: _selectedDate, term: _searchTerm);
        _bottomRevealController.close();
      },
      onDateSelect: (date) {
        setState(() {
          _selectedDate = date;
        });
        _fetchData(date: _selectedDate, term: _searchTerm);
      },
    );
  }
}
