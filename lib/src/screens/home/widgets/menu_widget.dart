import 'package:flutter/material.dart';

class MenuWidget extends StatelessWidget {
  final Function onInfoTap;
  final Function onCalendarTap;

  MenuWidget({@required this.onInfoTap, @required this.onCalendarTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GestureDetector(
          onTap: onInfoTap,
          child: _iconButton(icon: Icons.info),
        ),
        SizedBox(
          height: 20.0,
        ),
        GestureDetector(
          onTap: onCalendarTap,
          child: _iconButton(
            icon: Icons.today,
            iconColor: Colors.red[300],
          ),
        ),
      ],
    );
  }

  Widget _iconButton({IconData icon, Color iconColor = Colors.lightBlueAccent}) {
    return CircleAvatar(
      backgroundColor: Colors.grey[700],
      radius: 30.0,
      child: Icon(
        icon,
        size: 30.0,
        color: iconColor,
      ),
    );
  }
}
