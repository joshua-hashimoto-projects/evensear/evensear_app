import 'package:flutter/material.dart';

class SearchBarWidget extends StatelessWidget {
  final Function onTextChange;
  final Function onEditFinish;

  SearchBarWidget({@required this.onTextChange, @required this.onEditFinish});

  @override
  Widget build(BuildContext context) {
    return TextField(
      style: TextStyle(color: Colors.white, fontSize: 18.0),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey,
        contentPadding: EdgeInsets.all(16.0),
        prefixIcon: Icon(
          Icons.search,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          gapPadding: 8.0,
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
      onChanged: onTextChange,
      onEditingComplete: onEditFinish,
    );
  }
}
