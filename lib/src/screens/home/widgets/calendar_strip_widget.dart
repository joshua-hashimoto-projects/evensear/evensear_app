import 'package:calendar_strip/calendar_strip.dart';
import 'package:flutter/material.dart';

class CalendarStripWidget extends StatelessWidget {
  final DateTime _calendarStart = DateTime.now().subtract(Duration(days: 365 * 5));
  final DateTime _calendarEnd = DateTime.now().add(Duration(days: 365 * 5));
  final DateTime selectedDate;
  final Function onDateSelect;
  final Function onMonthTap;

  CalendarStripWidget({
    @required this.selectedDate,
    @required this.onDateSelect,
    @required this.onMonthTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: CalendarStrip(
        selectedDate: selectedDate,
        startDate: _calendarStart,
        endDate: _calendarEnd,
        iconColor: Colors.black87,
        monthNameWidget: _monthNameWidget,
        dateTileBuilder: _dateTileWidget,
        onDateSelected: onDateSelect,
        containerDecoration: BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.all(
            Radius.circular(15.0),
          ),
        ),
        // containerHeight: 100.0,
      ),
    );
  }

  Widget _monthNameWidget(String monthName) {
    return Container(
      padding: EdgeInsets.only(top: 8, bottom: 4),
      child: GestureDetector(
        onTap: onMonthTap,
        child: Text(
          monthName,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black87,
          ),
        ),
      ),
    );
  }

  Widget _dateTileWidget(date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {
    bool isSelectedDate = date.compareTo(selectedDate) == 0;
    Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.white;
    TextStyle normalStyle = TextStyle(
      fontSize: 17,
      fontWeight: FontWeight.w800,
      color: fontColor,
    );
    TextStyle selectedStyle = TextStyle(
      fontSize: 17,
      fontWeight: FontWeight.w800,
      color: Colors.black87,
    );
    List<Widget> _children = [
      Text(
        dayName,
        style: TextStyle(
          fontSize: 14.5,
          color: isSelectedDate ? Colors.black87 : Colors.white,
        ),
      ),
      Text(
        date.day.toString(),
        style: !isSelectedDate ? normalStyle : selectedStyle,
      ),
    ];
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
      decoration: BoxDecoration(
        color: !isSelectedDate ? Colors.transparent : Colors.white70,
        borderRadius: BorderRadius.all(Radius.circular(60)),
      ),
      child: Column(
        children: _children,
      ),
    );
  }
}
