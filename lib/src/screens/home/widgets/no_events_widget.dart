import 'package:flutter/material.dart';

class NoEventsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          'イベントはありません',
          style: TextStyle(
            color: Colors.grey[500],
          ),
        ),
      ),
    );
  }
}
