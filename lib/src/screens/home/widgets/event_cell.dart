import 'package:evensear_app/src/models/event.dart';
import 'package:evensear_app/src/screens/detail/detail_screen.dart';
import 'package:flutter/material.dart';

class EventCell extends StatelessWidget {
  final Event use;
  final Radius cellRadius = Radius.circular(10.0);

  EventCell({this.use});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailScreen(
              use: use,
            ),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        margin: EdgeInsets.only(bottom: 20.0),
        height: 250.0,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Hero(
                tag: use.eventId,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: _img(
                        uri: use.imagePath,
                      ),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(cellRadius),
                    boxShadow: [
                      _cellShadow(),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(20.0),
                margin: EdgeInsets.only(
                  top: 20.0,
                  bottom: 20.0,
                ),
                child: Column(
                  children: <Widget>[
                    Flexible(
                      child: Text(use.title),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: cellRadius,
                    bottomRight: cellRadius,
                  ),
                  color: Colors.white,
                  boxShadow: [
                    _cellShadow(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ImageProvider _img({String uri}) {
    if (uri.isEmpty || uri.contains('.pdf')) {
      return AssetImage('assets/img/no_image.png');
    } else {
      return NetworkImage(uri);
    }
  }

  BoxShadow _cellShadow() {
    return BoxShadow(
      color: Colors.grey,
      offset: Offset(5.0, 5.0),
      blurRadius: 10.0,
    );
  }
}
