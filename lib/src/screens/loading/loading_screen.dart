import 'dart:async';
import 'package:evensear_app/src/apis/eventon.dart';
import 'package:evensear_app/src/blocs/events_bloc.dart';
import 'package:evensear_app/src/screens/home/home_screen.dart';
import 'package:evensear_app/src/utils/custom_datetime_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  final _eventon = Eventon();

  Future fetchData() async {
    final events =
        await _eventon.fetchEvents(datetime: CustomDatetimeFormat.plainYmd(DateTime.now()));
    eventsBloc.changeEvents(events);
  }

  void moveToHome() async {
    await fetchData();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

  @override
  void initState() {
    super.initState();
    moveToHome();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: SpinKitDoubleBounce(
          color: Colors.white,
          size: 60.0,
        ),
      ),
    );
  }
}
