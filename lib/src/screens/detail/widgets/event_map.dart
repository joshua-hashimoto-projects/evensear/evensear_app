import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class EventMap extends StatefulWidget {
  final double lat;
  final double lng;

  EventMap({this.lat, this.lng});

  @override
  _EventMapState createState() => _EventMapState();
}

class _EventMapState extends State<EventMap> {
  Completer<GoogleMapController> _mapController = Completer();

  List<Marker> markers = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      markers.add(Marker(
          markerId: MarkerId('${widget.lat}_${widget.lng}'),
          draggable: true,
          position: LatLng(widget.lat, widget.lng)));
    });
  }

  void _onMapCreate(GoogleMapController controller) {
    _mapController.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      onMapCreated: _onMapCreate,
      initialCameraPosition: CameraPosition(
        target: LatLng(widget.lat, widget.lng),
        zoom: 14.0,
      ),
      markers: Set<Marker>.from(markers),
    );
  }
}
