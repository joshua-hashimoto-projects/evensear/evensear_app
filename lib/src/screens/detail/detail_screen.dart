import 'package:evensear_app/src/models/event.dart';
import 'package:evensear_app/src/screens/detail/widgets/event_map.dart';
import 'package:evensear_app/src/utils/custom_datetime_format.dart';
import 'package:evensear_app/src/utils/utils.dart';
import 'package:evensear_app/src/widgets/back_icon_button_widget.dart';
import 'package:evensear_app/src/widgets/custom_icon_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailScreen extends StatelessWidget {
  final Event use;

  DetailScreen({this.use});

  // void _launch({String uri}) async {
  //   if (await canLaunch(uri)) {
  //     await launch(uri);
  //   } else {
  //     throw 'Could not Launch $uri';
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              child: Stack(
                children: <Widget>[
                  _eventImage(context),
                  _eventDetail(),
                ],
              ),
            ),
          ),
          BackIconButtonWidget(),
          _openEventonUrl(context),
        ],
      ),
    );
  }

  Widget _eventImage(BuildContext context) {
    return Hero(
      tag: use.eventId,
      child: Container(
        height: 300,
        width: MediaQuery.of(context).size.width,
        child: _img(uri: use.imagePath),
      ),
    );
  }

  Widget _img({String uri}) {
    if (uri.isEmpty || uri.contains('.pdf')) {
      return Image.asset(
        'assets/img/no_image.png',
        fit: BoxFit.cover,
      );
    } else {
      return Image.network(
        uri,
        fit: BoxFit.cover,
      );
    }
  }

  Widget _eventDetail() {
    return Container(
      margin: EdgeInsets.fromLTRB(16.0, 250.0, 16.0, 16.0),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _eventSnippetInfo(),
          SizedBox(
            height: 30.0,
          ),
          _eventContent(),
          SizedBox(
            height: 30.0,
          ),
          _eventMap(),
        ],
      ),
    );
  }

  Widget _eventSnippetInfo() {
    final String startDate = CustomDatetimeFormat.slashYmd(DateTime.parse(use.startedAt).toLocal());
    final String startTime =
        CustomDatetimeFormat.plainTime(DateTime.parse(use.startedAt).toLocal());
    final String endTime = CustomDatetimeFormat.plainTime(DateTime.parse(use.endedAt).toLocal());
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              child: SelectableText(
                use.title,
                style: TextStyle(fontSize: 30.0),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10.0,
        ),
        ListTile(
          leading: Icon(Icons.calendar_today),
          title: Text(startDate),
          subtitle: Text('$startTime - $endTime'),
        ),
        use.place.isNotEmpty && use.address.isNotEmpty
            ? ListTile(
                leading: Icon(Icons.place),
                title:
                    use.place.isNotEmpty ? SelectableText(use.place) : SelectableText(use.address),
                subtitle: use.place.isNotEmpty ? SelectableText(use.address) : null,
              )
            : ListTile(
                leading: Icon(Icons.place),
                title: Text(
                  'No info',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ),
        ListTile(
          leading: Icon(Icons.people),
          title: Text('参加 ${use.accepted} / 定員 ${use.capacity}'),
        ),
      ],
    );
  }

  Widget _eventContent() {
    return Column(
      children: <Widget>[
        rowTitle(title: '詳細'),
        Center(
          child: Html(
            data: use.contents,
            // padding: EdgeInsets.all(16.0),
          ),
        ),
      ],
    );
  }

  Widget _eventMap() {
    try {
      final double eventLat = double.parse(use.lat);
      final double eventLng = double.parse(use.lng);
      return Column(
        children: <Widget>[
          rowTitle(title: 'アクセス'),
          use.place.isNotEmpty && use.address.isNotEmpty
              ? ListTile(
                  title: use.place.isNotEmpty
                      ? SelectableText(use.place)
                      : SelectableText(use.address),
                  subtitle: use.place.isNotEmpty ? SelectableText(use.address) : null,
                )
              : Container(),
          AspectRatio(
            aspectRatio: 4 / 3,
            child: EventMap(
              lat: eventLat,
              lng: eventLng,
            ),
          ),
        ],
      );
    } catch (err) {
      print(err);
      return Container();
    }
  }

  Widget _openEventonUrl(BuildContext context) {
    return Positioned(
      top: 40.0,
      right: 10.0,
      child: CustomIconButtonWidget(
        icon: Icons.language,
        onTap: () => urlLaunch(uri: use.eventUrl),
      ),
    );
  }

  Widget rowDivider() {
    return Divider(
      height: 1.0,
      thickness: 2.0,
    );
  }

  Widget rowTitle({String title}) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        rowDivider(),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}
