import 'package:evensear_app/src/screens/info/acknowledgement_screen.dart';
import 'package:evensear_app/src/screens/info/app_info_screen.dart';
import 'package:evensear_app/src/screens/info/privacy_policy_screen.dart';
import 'package:evensear_app/src/screens/info/terms_and_conditions_screen.dart';
import 'package:evensear_app/src/widgets/custom_icon_button_widget.dart';
import 'package:flutter/material.dart';

class InfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.grey.shade200,
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 100.0),
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text('アプリ情報'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AppInfoScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  title: Text('利用規約'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndConditionsScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  title: Text('プライバシーポリシー'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PrivacyPolicyScreen(),
                      ),
                    );
                  },
                ),
                ListTile(
                  title: Text('Acknowledgement'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AcknowledgementScreen(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          _closeInfoWidget(context),
        ],
      ),
    );
  }

  Widget _closeInfoWidget(BuildContext context) {
    return Positioned(
      top: 40.0,
      left: 10.0,
      child: CustomIconButtonWidget(
        icon: Icons.close,
        onTap: () => Navigator.pop(context),
      ),
    );
  }
}
