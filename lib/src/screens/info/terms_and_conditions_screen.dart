import 'package:evensear_app/src/utils/utils.dart';
import 'package:evensear_app/src/widgets/back_icon_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class TermsAndConditionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 100.0),
            child: FutureBuilder(
              future:
                  DefaultAssetBundle.of(context).loadString('assets/docs/terms_and_conditions.md'),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                return snapshot.hasData
                    ? Markdown(
                        data: snapshot.data,
                        onTapLink: (uri) {
                          urlLaunch(uri: uri);
                        },
                      )
                    : Container();
              },
            ),
          ),
          BackIconButtonWidget(),
        ],
      ),
    );
  }
}
