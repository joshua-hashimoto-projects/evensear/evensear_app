class Event {
  final String eventId;
  final String title;
  final String summary;
  final String imagePath;
  final String contents; // HTML形式
  final String startedAt;
  final String endedAt;
  final String entryStartedAt;
  final String entryEndedAt;
  final String capacity;
  final String eventUrl;
  final String referenceUrl;
  final String prefectureId;
  final String address;
  final String place;
  final String lat;
  final String lng;
  final int accepted;
  final String embedCode; // HTML形式

  Event(Map<String, dynamic> decodedJson)
      : eventId = decodedJson['event_id'],
        title = decodedJson['title'],
        summary = decodedJson['summary'],
        imagePath = decodedJson['image_path'],
        contents = decodedJson['contents'],
        startedAt = decodedJson['started_at'],
        endedAt = decodedJson['ended_at'],
        entryStartedAt = decodedJson['entry_started_at'],
        entryEndedAt = decodedJson['entry_ended_at'],
        capacity = decodedJson['capacity'],
        eventUrl = decodedJson['event_url'],
        referenceUrl = decodedJson['url'],
        prefectureId = decodedJson['prefecture_id'],
        address = decodedJson['address'],
        place = decodedJson['place'],
        lat = decodedJson['lat'],
        lng = decodedJson['lng'],
        accepted = decodedJson['accepted'],
        embedCode = decodedJson['embed_code'];
}
