import 'dart:async';
import 'package:evensear_app/src/models/event.dart';
import 'package:evensear_app/src/utils/validator.dart';
import 'package:rxdart/rxdart.dart';

class EventsBloc extends Object with Validators {
  final _eventsController = BehaviorSubject<List<Event>>();

  // stream
  Stream<List<Event>> get events => _eventsController.stream.transform(validateEvents);

  // change data via sink
  Function(List<Event>) get changeEvents => _eventsController.sink.add;

  void dispose() {
    _eventsController.close();
  }
}

final eventsBloc = EventsBloc();
